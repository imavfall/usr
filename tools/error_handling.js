const error_handling = (e, res) => {
  console.log(e)
  switch (e.name) {
    case "SequelizeValidationError": 
      res.status(400).send(e);
      break;
    case "SequelizeUniqueConstraintError":
      res.status(409).send(e);
      break;
    default:
      res.status(500).send(e);
      break;
  }
} 
module.exports = error_handling;

