process.env.NODE_ENV = 'test'
const dotenv = require('dotenv');
const fs = require('fs');
const envConfig = dotenv.parse(fs.readFileSync('.env.test'))
// Warning will override env overlapping keys
for (const k in envConfig) {
  process.env[k] = envConfig[k]
}

const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../app');
const sqlFixtures = require('sql-fixtures');
const { expect } = require('chai');
const user = require('../models/User');


var dbConfig = {
        client: 'mysql',
        connection: {
        host     : process.env.DB_HOST,
        user     : process.env.DB_USER,
        port     : process.env.DB_PORT,
        password : process.env.DB_PASS,
        database : process.env.DB_NAME
    }
}
var knex = require('knex')(dbConfig);
var dataSpec = {
  User: {
    firstName: "Anthony",
    lastName: "Hopkins",
    email: "anthony.hopkins@club-internet.fr",
    created_date: "2018-09-24"
  }
};

const should = chai.should();
chai.use(chaiHttp)
describe('/GET user', () => {
  beforeEach(async () => {
    await knex('Like').del()
    await knex('Caracteristic').del()
    await knex('Message').del()
    await knex.raw('DELETE FROM `Match`')
    await knex.raw('ALTER TABLE ' + '`Match`' + ' AUTO_INCREMENT = 1')
    await knex('User').del()
    await knex.raw('ALTER TABLE ' + 'User' + ' AUTO_INCREMENT = 1')

    await sqlFixtures.create(dbConfig, dataSpec);
  });
    it('it should Get all users', (done) => {
        chai.request(app)
        .get('/user')
        .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('array');
            expect(res.body.length).to.equal(1)
            done();
          });
      });
      it('it should get a specific user', (done) => {
        chai.request(app)
        .get('/user/1')
        .end((err, res) => {
          console.log(res.body)
          if (res.body) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            expect(res.body.firstName).to.equal('Anthony')
            done();
          }
          });
      });
      it('it should post a user', (done) => {
        chai.request(app)
        .post('/user').send({
          firstName: "Anthony",
          lastName: "Hopkins",
          email: "anthony2.hopkins@club-internet.fr",
          created_date: "2018-09-24"
        })
        .end((err, res) => {
          res.should.have.status(200);
          done();
          });
      });
  });

