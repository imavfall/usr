'use strict';
module.exports = function(app) {
  var userController = require('../controllers/UserController');

  // userController Routes
  app.route('/user')
    .get(userController.list_all_users)

   app.route('/user/:id')
    .get(userController.read_a_user)

   app.route('/user')
    .post(userController.create_a_user)

   app.route('/user/:id')
    .put(userController.update_a_user)

};

