const { DataTypes } = require("sequelize");
const sequelize = require('../config/dev');
const User = require('../models/User.js');
const error_handling = require('../tools/error_handling.js');
const userInstance = User(sequelize, DataTypes);


exports.list_all_users = async function(req, res) {
  try {
    var user = await userInstance.findAll();
    res.json(user);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.read_a_user = async function(req, res) {
  try {
    var user = await userInstance.findOne({where: {
    id: req.params.id
  }});
    res.json(user);
  } catch (e) {
    error_handling(e, res)
  }
};


exports.create_a_user = async function(req, res) {
  try {
    var user = await userInstance.create(req.body);
    res.json(user);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.update_a_user = async function(req, res) {
  try {
    var user = await userInstance.update(req.body, {
      where: {
        id: req.params.id
      }
    });
    res.json(req.body);
  } catch (e) {
    error_handling(e, res);
  }
};

