const express = require('express'),
app = express(),
bodyParser = require('body-parser');
require('dotenv').config()
const port = process.env.PORT || 3000;
app.listen(port);
console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./routes/routes.js'); //importing route
routes(app);

module.exports = app;

